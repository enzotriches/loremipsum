<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lorem Ipsum</title>
	
	<meta name="description" content="">
	<meta name="author" content="Enzo Trichês">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="./Assets/bin/css/login.css">
    <link rel="stylesheet" href="./Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
 	<link rel="stylesheet" href="./Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">

  	<script src="./Assets/bin/js/jquery-3.2.1.min.js"></script>

</head>
<body>
	<!-- Nav -->
	<nav class="navbar navbar-fixed-top bg-light">
	 	<div class="container" id="ContainerHeader">
			<a class="navbar-brand a-logo" href="#">
				<img src="./Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
	   			<span class="h3 text-dark b">Lorem Ipsum</span>
	   		</a>
		</div>
	</nav>
	
	<section class="container-fluid text-center ml-2 mt-5" id="section-form-login">
			<form class="form-signin justify-content-right bg-light border rounded" method="POST" action="#">
			    <div class="text-center mb-4">
			      <img class="mb-4" src="../imagens/icons/bootstrap.png" alt="" width="72" height="72">
			      <h1 class="h1 mb-3 font-weight-normal">Entrada</h1>
			      <p>
			        	
			        	Caso não seja cadastrado, <a href="#Cadastre-se" class="link text-dark"> <u> cadastre-se. </u></a>
	 	          </p>
			   	  
					<small class="small text-secondary">
						<?php echo "Boas compras!."; ?>
					</small>
							
					
			    </div>
					
			      <div class="form-label-group m-1">
			      	<label for="email">Email:</label>
			        <input type="text" id="inputEmail" class="form-control" placeholder="loremipsum@gmail.com" name="email" required="" autofocus="" min="1" max="50">
			      </div>

			      <div class="form-label-group m-1">
			      	<label for="senha">Senha:</label>
			        <input type="password" id="inputPassword" class="form-control" placeholder=" * * * * * *" name="senha" required="" min="1" max="120">
			      </div>

			      <div class="checkbox mb-3">
			        <label>
			          <input type="checkbox" value="remember-me">
			           Lembre de mim
			        </label>
			      </div>
			      <input class="btn btn-lg btn-dark btn-block" name="goAdminLogin" type="submit" value="Entrar">
					<small class="small"><a href="../Shop/Galeria">Entrar</a></small>
			      <p class="mt-5 mb-3 text-muted text-center">® Lorem Ipsum</p>
	    	</form>
    	
	</section>

	<script src="./Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="./Assets/bin/js/pace.min.js"></script>
</body>
</html>