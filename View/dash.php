<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Secret</title>
	
	<meta name="description" content="">
	<meta name="author" content="Enzo Trichês">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="./Assets/bin/css/secret.css"> 

	<link rel="stylesheet" type="text/css" href="./Assets/bin/css/pace.css"> 
    <link rel="stylesheet" href="./Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

  	<script src="./Assets/bin/js/jquery-3.2.1.min.js"></script>

	
</head>
<body>
<!-- Nav -->
	<nav class="navbar navbar-fixed-top bg-black">
	 	<div class="container " id="ContainerHeader">

			<a class="navbar-brand a-logo justify-content-start text-center" href="#">
				<img src="./Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
	   			<span class="h3 text-light b">Lorem Ipsum</span>
	   		</a>
	   		<a role="button" class="btn btn-black border-light text-light justify-content-center" data-toggle="collapse" data-target="#sidebar" aria-expanded="true">
              <i class="fas fa-bars text-light"></i> 
            </a>
		</div>
	</nav>

<!-- Wrapper (Menu, Main)   -->
	<div class="container-fluid">
	    <div class="row d-flex d-md-block flex-nowrap wrapper">
	    	<!-- SIDEBAR -->
	        <div class="col-md-2 float-left col-1 pl-0 pr-0 collapse width show" id="sidebar">
	            <div class="list-group border-0 card text-center text-md-left"> 				 

	            <!-- Menu -->
	                <a href="index" class="list-group-item d-inline-block collapsed">
	                	<i class="fa fa-tachometer-alt text-white"></i> 
	                	<span class="d-none d-md-inline">Menu</span>

	                </a>

				<!-- Produtos -->
	                <a href="#produtos" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fas fa-box text-warning"></i> 
	                	<span class="d-none d-md-inline">Produtos</span>
	                	<span class="badge badge-warning">2</span> 
	                </a>
	                	<!-- Data -->
		                <div class="collapse" id="produtos" data-parent="#sidebar">
		                    <a href="#" class="list-group-item">Coleções</a>
		                    <a href="#" class="list-group-item">Avisos</a>
		                </div>
		        <!-- Pedidos -->
	                <a href="#pedidos" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fa fa-shopping-cart text-success"></i> 
	                	<span class="d-none d-md-inline">Pedidos</span>
	                	<span class="badge badge-success">14</span> 
	                </a>
						<!-- Menu -->
		                <div class="collapse" id="pedidos" data-parent="#sidebar">
		                	<!-- Compras -->
		                    <a href="#pedidos_compras" class="list-group-item" data-toggle="collapse" aria-expanded="false">Compras</a>
		                    <div class="collapse" id="pedidos_compras">
		                        <a href="#" class="list-group-item" data-parent="#menu3sub2">Ativas</a>
		                        <a href="#" class="list-group-item" data-parent="#menu3sub2">Concluidas</a>
		                        <a href="#" class="list-group-item" data-parent="#menu3sub2">Canceladas</a>
		                    </div>
		                    <!-- Atendimento -->
		                    <a href="#" class="list-group-item" data-parent="#menu3">Atendimento</a>
		                </div>
		        <!-- Usuários -->
		            <a href="#usuarios" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fa fa-user text-primary"></i> 
	                	<span class="d-none d-md-inline">Usuários</span>
	                	<span class="badge badge-primary">6</span> 
	                </a>
		                <div class="collapse" id="usuarios" data-parent="#sidebar">
		                    <a href="#" class="list-group-item" data-parent="#menu3">
		                    	Clientes
		                    </a>
		                    <a href="#" class="list-group-item" data-parent="#menu3">
		                    	Equipe
		                    </a>
		                    <a href="#" class="list-group-item" data-parent="#menu3">
		                    	Você
		                    </a>
		                </div>
		        <!-- Marketing -->
		            <a href="#marketing" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fa fa-eye text-purple"></i> 
	                	<span class="d-none d-md-inline">Marketing</span> 
	                </a>
		                <div class="collapse" id="marketing" data-parent="#sidebar">
		                    <a href="#" class="list-group-item" data-parent="#marketing">Informações gerais</a>
		                    <a href="#" class="list-group-item" data-parent="#marketing">Cupoms</a>
		                    <a href="#" class="list-group-item" data-parent="#marketing">Email</a>
		                    <a href="#" class="list-group-item" data-parent="#marketing">Social</a>
		                </div>
	            <!-- Sair -->
	                <a href="#" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
	                	<i class="fa fa-door-open text-danger"></i> 
	                	<span class="d-none d-md-inline">Sair</span>
	                </a>
	            </div>
	        </div>
	        <!-- CONTEUDO -->
	        <main class="col-md-10 float-left col px-5 pl-md-2 pt-2 main bg-light">
	        	<!-- Onde estou -->
	        	<div class="row">
	        		<!-- Titulo ['Option'] -->
	        		<div class="col-12">
	        			
			            <!-- Titulo ['Option'] -->
			            <div class="page-header">
			            	<i class="fa fa-tachometer-alt fa-2x text-dark">
			               	
			               </i>
				            <span class="h3">
				               
				                	DashBoard
				            </span>
			            </div>
	        		</div>
	        		<!-- Olá e texto -->
	        		<div class="col-12">
			            <p class="lead ">  Bem vindo a área administrativa, Enzo Trichês.</p>
			            
	        			        		
	        		</div>
	        	</div>
	        		<hr>
	            <div class="row">
	                <div class="col-12 col-lg-6">
	                    
	                </div>
	            </div>
	            <hr>
	            <!--  -->
	        </main>
	    </div>    
	</div>

<!-- Footer -->
	<footer class="container-fluid bg-black text-center justify-content-center pt-1 pb-1" style="clear:both;">
				<h1 class="text-light text-center">
					Footer
				</h1>
	</footer>
	
	<!--  -->
	<script src="./Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="./Assets/bin/js/pace.min.js"></script>
</body>
</html>