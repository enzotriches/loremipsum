<?php 
spl_autoload_register(function($class){
	if (file_exists('./Controllers/'.$class.'.php')) {
		require_once('./Controllers/'.$class.'.php');
	}
	if (file_exists('./Models/'.$class.'.php')) {
		require_once('./Models/'.$class.'.php');
	}
});
	/*echo "<pre>";
	
	$control = new Controller();
	var_dump($control);
	

	echo "<br>";
	$user = new Usuario();

	
	$user->loadById('1');
	echo $user;


	echo "</pre>";*/
 ?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Lorem Ipsum</title>
	<meta name="description" content="">
	<meta name="author" content="Enzo Trichês">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="../../Assets/bin/css/index.css">
 	<link rel="stylesheet" href="./Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
  	<script src="../Assets/bin/js/jquery-3.2.1.min.js"></script>

</head>
<body>
	
<!-- Nav -->
	<nav class="navbar navbar-fixed-top navbar-expand-lg bg-light">
	 	<div class="container" id="ContainerHeader">
	 		<!-- Slogan -->
			<a class="navbar-brand a-logo" href="#">
				<img src="../Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
	   			<span class="h3 text-dark b">Lorem Ipsum</span>
	   		</a>
	   		<!-- Botão Menu -->
			<button class="navbar-toggler bg-dark" type="button" data-toggle="collapse" data-target="#navbarSite">
				<span class="navbar-toggler-icon justify-content-center h-auto w-auto text-light b ">
					Menu
				</span>
			</button>
			<!-- Content Menu  -->
			<div class="collapse navbar-collapse justify-content-md-center" id="navbarSite">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<ul class="navbar-nav mr-1 mt-2 ">
							<li class="nav-item dropdown mr-lg-3 ml-1 b">
								<a href="#" class="span text-dark b" id="home"><b>Home</b></a>
							</li>
							<li class="nav-item dropdown mr-lg-3 ml-1">
								<a href="#" class="span text-dark " id="sobre">Sobre</a>
							</li>
							<li class="nav-item dropdown mr-lg-3 ml-1 ">
								<a href="#" class="span text-dark " id="contato">Contato</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a href="Entrada/" class="btn bg-dark text-light " id="comprar">
							Área varejista
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
<!-- /Nav -->
<!-- Container -->
	<div class="container justify-content-center text-center mt-5 pt-5">
		<h1 class="h1 b text-black pt-2 mt-5">
			Lorem Ipsum
		</h1>
		<p class="h2 b pt-3 mb-5 text-dark">
			Dolor sit amet sudo apet get...
		</p>
		<a href="#" class="btn btn-light mt-2 text-dark rounded border-secondary btn-lg">
			Saber mais! 
		</a>
	</div>
<!-- /Container -->
 
	<script src="../Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../Assets/bin/js/pace.min.js"></script>
</body>
</html>
