<?php 
require_once('./Controllers/Controller.php');

class Usuario{

	private $id;
	private $nome;
	private $sobrenome;
	protected $control;
	protected $table;
	protected $columns;

	public function __construct($id,$nome,$sobrenome){
		$this->control = new Controller();
		$this->table = 'usuario';
		$this->columns = array('id','nome','sobrenome');
		$this->id = $id;
		$this->nome = $nome;
		$this->sobrenome = $sobrenome;
	}

	public function __toString(){
		return json_encode(
			array(
				'id' => $this->getId(),
				'nome' => $this->getNome(),
				'sobrenome' => $this->getSobrenome()
			)
		);
	}

	public function getId(){
		return $this->id;
	}
	public function getNome(){
		return $this->nome;
	}

	public function getSobrenome(){
		return $this->sobrenome;
	}
	public function getTable(){
		return $this->table;
	}

	public function getData(){
		return json_encode(array(
			'tb' => $this->getTable(),
			'id' => $this->getId(),
			'nome' => $this->getNome(),
			'sobrenome' => $this->getSobrenome(),
		));
	}


	public function setId($id){
		$this->id = $id;
	}
	public function setNome($nome){
		$this->nome = $nome;
	}
	public function setSobrenome($sobrenome){
		$this->sobrenome = $sobrenome;
	}



	public function loadById($id){

		$result = $this->control->select("SELECT * FROM usuario WHERE id = ?", array(
			
			'1' => $id
		));

		if (count($result)>0) {
			
			if (isset($result[0])) {

				$row = $result[0];

				$this->setId($row['id']);
				$this->setNome($row['nome']);
				$this->setSobrenome($row['sobrenome']);
			}

		}
	}
	public function verifyColumnsExistence($data = array()){

		foreach ($this->columns as $key => $value) {
			foreach ($data as $cat => $chup) {
				if ($key['value'] === $catchup) {
					
				}
			}
		}
	}

	public function cadastrar($that){
		
			return $this->control->create($that);
			
	}
}

	

?>
