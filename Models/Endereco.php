<?php


namespace Lojista\Endereco;

/**
 * Description of Endereco
 *
 * @author Enzo Trichês
 */
class Endereco {
    private $id_end;
    private $id_loja;
    private $logradouro;
    private $numero;
    private $complemento;
    private $cep;
    private $bairro;
    private $uf;
    private $municipio;
    
    public function __construct($id_end, $id_loja, $logradouro, $numero, $complemento, $cep, $bairro, $uf, $municipio) {
        $this->id_end = $id_end;
        $this->id_loja = $id_loja;
        $this->logradouro = $logradouro;
        $this->numero = $numero;
        $this->complemento = $complemento;
        $this->cep = $cep;
        $this->bairro = $bairro;
        $this->uf = $uf;
        $this->municipio = $municipio;
    }
    
    public function getId_end() {
        return $this->id_end;
    }

    public function getId_loja() {
        return $this->id_loja;
    }

    public function getLogradouro() {
        return $this->logradouro;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getComplemento() {
        return $this->complemento;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getUf() {
        return $this->uf;
    }

    public function getMunicipio() {
        return $this->municipio;
    }

    public function setId_end($id_end) {
        $this->id_end = $id_end;
        return $this;
    }

    public function setId_loja($id_loja) {
        $this->id_loja = $id_loja;
        return $this;
    }

    public function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
        return $this;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
        return $this;
    }

    public function setComplemento($complemento) {
        $this->complemento = $complemento;
        return $this;
    }

    public function setCep($cep) {
        $this->cep = $cep;
        return $this;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
        return $this;
    }

    public function setUf($uf) {
        $this->uf = $uf;
        return $this;
    }

    public function setMunicipio($municipio) {
        $this->municipio = $municipio;
        return $this;
    }


    
    
}
