<?php


namespace Lojista\Documento;

/**
 * Description of Documento
 *
 * @author Enzo Trichês
 */
class Documento {
    
    private $id_documento;
    private $descricao;
    private $tipo;
    private $lojista;
    
    public function __construct($id_documento, $descricao, $tipo, $lojista) {
        $this->id_documento = $id_documento;
        $this->descricao = $descricao;
        $this->tipo = $tipo;
        $this->lojista = $lojista;
    }

    public function getId_documento() {
        return $this->id_documento;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getLojista() {
        return $this->lojista;
    }

    public function setId_documento($id_documento) {
        $this->id_documento = $id_documento;
        return $this;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
        return $this;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
        return $this;
    }

    public function setLojista($lojista) {
        $this->lojista = $lojista;
        return $this;
    }


    
}
