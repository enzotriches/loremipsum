<?php

namespace Lojista\Lojista;

/**
 * Description of Lojista
 *
 * @author Enzo Trichês
 */
class Lojista {
 
    private $id_loja;
    private $responsavelUser;
    private $nome_empresarial;
    private $cnpj;
    private $email;
    private $login;
    private $senha;
    private $insc_estadual;
    private $isento;
    private $razao_social;
    private $status;
    private $sendEmails;
    private $data_cad;
    
    
    function __construct($id_loja, $responsavelUser, $nome_empresarial, $cnpj, $email, $login, $senha, $insc_estadual, $isento, $razao_social, $status, $sendEmails, $data_cad) {
        $this->id_loja = $id_loja;
        $this->responsavelUser = $responsavelUser;
        $this->nome_empresarial = $nome_empresarial;
        $this->cnpj = $cnpj;
        $this->email = $email;
        $this->login = $login;
        $this->senha = $senha;
        $this->insc_estadual = $insc_estadual;
        $this->isento = $isento;
        $this->razao_social = $razao_social;
        $this->status = $status;
        $this->sendEmails = $sendEmails;
        $this->data_cad = $data_cad;
    }
    public function getId_loja() {
        return $this->id_loja;
    }

    public function getResponsavelUser() {
        return $this->responsavelUser;
    }

    public function getNome_empresarial() {
        return $this->nome_empresarial;
    }

    public function getCnpj() {
        return $this->cnpj;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getLogin() {
        return $this->login;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function getInsc_estadual() {
        return $this->insc_estadual;
    }

    public function getIsento() {
        return $this->isento;
    }

    public function getRazao_social() {
        return $this->razao_social;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getSendEmails() {
        return $this->sendEmails;
    }

    public function getData_cad() {
        return $this->data_cad;
    }

    public function setId_loja($id_loja) {
        $this->id_loja = $id_loja;
        return $this;
    }

    public function setResponsavelUser($responsavelUser) {
        $this->responsavelUser = $responsavelUser;
        return $this;
    }

    public function setNome_empresarial($nome_empresarial) {
        $this->nome_empresarial = $nome_empresarial;
        return $this;
    }

    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setLogin($login) {
        $this->login = $login;
        return $this;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
        return $this;
    }

    public function setInsc_estadual($insc_estadual) {
        $this->insc_estadual = $insc_estadual;
        return $this;
    }

    public function setIsento($isento) {
        $this->isento = $isento;
        return $this;
    }

    public function setRazao_social($razao_social) {
        $this->razao_social = $razao_social;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setSendEmails($sendEmails) {
        $this->sendEmails = $sendEmails;
        return $this;
    }

    public function setData_cad($data_cad) {
        $this->data_cad = $data_cad;
        return $this;
    }


}
