<?php

namespace Lojista\LojistaResponsavel;

/**
 * Description of LojistaResponsavel
 *
 * @author Enzo Trichês
 */
class LojistaResponsavel {
    
    private $id_lojistaResponsavel;
    private $nome_completo;
    private $telefone;
    private $cpf;
    private $email;
    private $codigo_de_login;
    
    public function __construct($id_lojistaResponsavel, $nome_completo, $telefone, $cpf, $email, $codigo_de_login) {
        $this->id_lojistaResponsavel = $id_lojistaResponsavel;
        $this->nome_completo = $nome_completo;
        $this->telefone = $telefone;
        $this->cpf = $cpf;
        $this->email = $email;
        $this->codigo_de_login = $codigo_de_login;
    }
    public function getId_lojistaResponsavel() {
        return $this->id_lojistaResponsavel;
    }

    public function getNome_completo() {
        return $this->nome_completo;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getCpf() {
        return $this->cpf;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCodigo_de_login() {
        return $this->codigo_de_login;
    }

    public function setId_lojistaResponsavel($id_lojistaResponsavel) {
        $this->id_lojistaResponsavel = $id_lojistaResponsavel;
        return $this;
    }

    public function setNome_completo($nome_completo) {
        $this->nome_completo = $nome_completo;
        return $this;
    }

    public function setTelefone($telefone) {
        $this->telefone = $telefone;
        return $this;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setCodigo_de_login($codigo_de_login) {
        $this->codigo_de_login = $codigo_de_login;
        return $this;
    }

    

    
    
    
}
