<?php
namespace Lojista\Notificacao;
/**
 * Description of Notificacao
 *
 * @author Enzo Trichês
 */
class Notificacao {
    //put your code here
    private $id_notificacao;
    private $titulo;
    private $conteudo;
    private $nivel;
    private $lojista;
    private $status;
    private $des_data;
    
    public function __construct($id_notificacao, $titulo, $conteudo, $nivel, $lojista, $status, $des_data) {
        $this->id_notificacao = $id_notificacao;
        $this->titulo = $titulo;
        $this->conteudo = $conteudo;
        $this->nivel = $nivel;
        $this->lojista = $lojista;
        $this->status = $status;
        $this->des_data = $des_data;
    }
    public function getId_notificacao() {
        return $this->id_notificacao;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getConteudo() {
        return $this->conteudo;
    }

    public function getNivel() {
        return $this->nivel;
    }

    public function getLojista() {
        return $this->lojista;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getDes_data() {
        return $this->des_data;
    }

    public function setId_notificacao($id_notificacao) {
        $this->id_notificacao = $id_notificacao;
        return $this;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
        return $this;
    }

    public function setConteudo($conteudo) {
        $this->conteudo = $conteudo;
        return $this;
    }

    public function setNivel($nivel) {
        $this->nivel = $nivel;
        return $this;
    }

    public function setLojista($lojista) {
        $this->lojista = $lojista;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setDes_data($des_data) {
        $this->des_data = $des_data;
        return $this;
    }


    
}
