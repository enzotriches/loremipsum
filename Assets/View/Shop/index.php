<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lorem Ipsum</title>
	
	<meta name="description" content="">
	<meta name="author" content="Enzo Trichês">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="../Assets/bin/css/shop.css"> 
    <link rel="stylesheet" href="../Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">

  	<script src="../Assets/bin/js/jquery-3.2.1.min.js"></script>

</head>
<body>
	<!-- Nav -->
	<nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark  bg-dark" id="headerNav">
			<div class="container" id="ContainerHeader">
				<a class="navbar-brand a-logo" href="#">
					<img src="../Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
		   			<span class="h4">Lorem Ipsum</span>
		   		</a>
				
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
					<span class="navbar-toggler-icon"></span>
				</button>
			
				<div class="collapse navbar-collapse justify-content-md-center" id="navbarSite">
					
					<form action="#" class="form-inline ml-3 mr-0" id="FormHeader">
						<input type="text" class="form-control mr-2" id="buscar-header" type="search" placeholder="Buscar..." />
					</form>

					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							
							<a href="#" class="nav-link dropdown-toggle " data-toggle="dropdown" id="navDrop">
								Olá, random!
							</a>

							<div class="dropdown-menu">
								
								<a href="#Perfil" class="dropdown-item">Perfil</a>
								<a href="./Carrinho/" class="dropdown-item">Carrinho <span class="badge badge-danger">3</span></a>
								<a href="#Notificações" class="dropdown-item">Notificações <span class="badge badge-secondary">1</span></a>
								<a href="#Sair" class="dropdown-item">Sair</a>

							</div>

						
						</li>
					</ul>

				</div>
			</div>
	</nav>	
	<!-- Filtro -->
	<nav class="navbar container navbar-light bg-light rounded mt-3 border mb-2" id="filtro">
			<div class="row menuButton">
					<div class="col col-md-12">
						<button class="navbar-toggler mr-0" type="button" data-toggle="collapse" data-target="#FiltroMenu" aria-controls="FiltroMenu" aria-expanded="true" aria-label="Toggle navigation">
				        <span class="navbar-toggler-icon"></span> Filtrar pesquisa
				      	</button>
			      	</div>
		    </div>
			<div class="navbar-collapse collapse show" id="FiltroMenu">
			 	<form action="">
			      	<!-- Filtro Row-1 (Representante, Coleção, Categoria) -->
			      	<div class="row justify-content-center">
			      		<!-- Representante Item Filtro -->
			      		<div class="col col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12 itemFiltro">
			      			<div class="span nomeFiltro" id="representanteF">Tamanho</div>
			      			<select class="form-control input-Filtro" name="representanteData">
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
 			      			</select>
			      		</div>
			      		<!-- Coleção Item Filtro -->
			      		<div class="col col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12 itemFiltro">
			      			<div class="span nomeFiltro" id="colecaoF">Coleção</div>
			      			<select class="form-control input-Filtro disable" name="colecaoData">
			      				<option value="1">Nome da coleção</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>	      				
				      		</select>
				      	</div>
			      		<!-- Categoria Item Filtro -->
			      		<div class="col col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12 itemFiltro">
			      			<div class="span nomeFiltro" id="categoriaF">Categoria</div>
			      			<select class="form-control input-Filtro disable" name="categoriaData" >
			      				<option value="1">Nome da categoria</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      			</select>
			      		</div>
			      	</div>
			      	<!-- Filtro Row-2 (Cor, Sexo, Listar por) -->
			      	<div class="row justify-content-center">
			      		<!-- Cor Item Filtro -->
			      		<div class="col col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12 itemFiltro">
			      			<div class="span nomeFiltro" id="corF">Cor</div>
			      			<select class="form-control input-Filtro" name="corData">
			      				<option value="">Selecione uma cor</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>			      				
			      			</select>
			      		</div>
			      		<!-- Sexo Item Filtro -->
			      		<div class="col col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12 itemFiltro">
			      			<div class="span nomeFiltro" id="sexoF">Sexo</div>
			      			<select class="form-control input-Filtro disable" name="sexoData">
			      				<option value="">Selecione um sexo</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>
			      				<option value="">Selecione um representante</option>		      				
		      			    </select>
			      		</div>
			      		<!-- Listar Por Item Filtro -->
			      		<div class="col col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12 itemFiltro">
			      			<div class="span nomeFiltro" id="listarPorF" >Listar por</div>
			      			<select class="form-control input-Filtro disable" name="listarPorData">
			      				<option value="1">Mais relevante</option>
			      				<option value="4">Recentes</option>
			      				<option value="2">Menor preço</option>
			      				<option value="3">Maior preço</option>
			      			</select>
			      		</div>
			      	</div>
			      	<!-- Filtro Row-3 (Preço(Min - Max)) -->
			     	<div class="row justify-content-center">
				 	<!-- Preço Item Filtro -->
			  			<div class="col justify-content-center col-12 col-lg-6 col-md-12 col-sm-12 col-xs-12 itemFiltro">
			    			<div class="span nomeFiltro text-center" id="corF">Valor</div>
		    				<div class="row">
		    					<div class="col col-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
		    						<input type="number" placeholder="Min" class="form-control input-Filtro" name="minData"  min="0" max="400" 
										value="0"
			    						/>
			    					</div>
		    					<div class="col col-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
		    						<input type="number" placeholder="Max" class="form-control input-Filtro" name="maxData" min="0" max="400" value="400"  />
		    					</div>
							</div>
			      		</div>
			      	</div>
			      	<!-- Botões Filtro -->
			      	<!-- Falta fazer o botão de pesquisar ou redefinir -->
			      	<div class="row justify-content-left">
			      		<div class="col col-4 col-lg-4 col-md-12 col-sm-12 col-xs-12">
							
			      			<input type="submit" name="GoFilter" class="btn btn-dark border btn-filter m-1 " value="Filtrar">
			      			<input type="reset" name="reset" class="btn btn-light border btn-reset m-1" value="Redefinir">
			      		</div>
			      	</div>
			    </form>
			</div>
	</nav>
	<!-- Galeria -->
	<section class="container boxProducts mt-1 text-center justify-content-center" id="produtoBox">	
			<div class="row justify-content-center">
				
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (11).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 01
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (6).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 06
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (4).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 05
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (6).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 06
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (11).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 01
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (6).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 06
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (4).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 05
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (6).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 06
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (11).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 01
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (6).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 06
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (4).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 05
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 col-md-4 text-center justify-content-center p-1 mb-1 border rounded bg-light">
					<img src="../Assets/bin/images/produtos/a (6).jpg" alt="" class="img border produto rounded">
						<div class="container bg-light rounded border-light">
							<div class="row">
								<div class="col-12">
									<h4 class="text-dark">
										Produto 06
									</h4>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-light border text-dark">
										Abrir
									</button>
								</div>
								<div class="col-6">
									<button class="btn col-12 btn-dark border text-light">
										Carrinho
									</button>
								</div>
							</div>
						</div>
					</img>
				</div>
			</div>
	</section>
	<!-- Footer -->
	<footer class="footer bg-dark text-center justify-content-center border mt-4 shadow p-1">
			<h1 class="text-light text-center">
				Footer
			</h1>
	</footer>

	<script src="../Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../Assets/bin/js/pace.min.js"></script>
</body>
</html>