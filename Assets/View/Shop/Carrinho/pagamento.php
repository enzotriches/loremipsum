<!DOCTYPE html>
<html>
<head>
	<!-- TITLE -->
	<title>Carrinho - Lorem Ipsum</title>
	<!-- CONFIGURATION  -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- CSS  -->
   	<link rel="stylesheet" href="../../../Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../../Assets/bin/css/cart.css"/>	
	<!-- JavaScript includes -->
	<script src="../../../Assets/bin/js/jquery-3.2.1.min.js"></script> 
	<script src="../../../Assets/bin/js/popper.js/dist/umd/popper.min.js"></script>
	<script src="../../../Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../../../Assets/bin/js/carrinho_dropdown.js"></script>
</head>

<body>
	<!-- Header -->
	<nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark  bg-dark" id="headerNav">
			<div class="container" id="ContainerHeader">
				<a class="navbar-brand a-logo" href="#">
					<img src="../../../Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
		   			<span class="h4">Lorem Ipsum</span>
		   		</a>
				
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
					<span class="navbar-toggler-icon"></span>
				</button>
			
				<div class="collapse navbar-collapse justify-content-md-center" id="navbarSite">
					
					<form action="#" class="form-inline ml-3 mr-0" id="FormHeader">
						<input type="text" class="form-control mr-2" id="buscar-header" type="search" placeholder="Buscar..." />
					</form>

					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							
							<a href="#" class="nav-link dropdown-toggle " data-toggle="dropdown" id="navDrop">
								Olá, random!
							</a>

							<div class="dropdown-menu">
								
								<a href="#Perfil" class="dropdown-item">Perfil</a>
								<a href="./Carrinho" class="dropdown-item">Carrinho <span class="badge badge-danger">3</span></a>
								<a href="#Notificações" class="dropdown-item">Notificações <span class="badge badge-secondary">1</span></a>
								<a href="#Sair" class="dropdown-item">Sair</a>

							</div>

						
						</li>
					</ul>

				</div>
			</div>
	</nav>	
	<!-- Container gatinho e sensual -->
	<section class="container border shadow p-3 box-cart mt-3 justify-content-center">
		<!-- Titulo ["Etapa"] -->
		<div class="row">
			<div class="col-12 text-center">
				<span class="h2 text-dark-50 b ">
					Visualização
				</span>
			</div>
		</div>
		<!-- Etapas ["Visualização"] -->
		<div class="row pb-1 pt-2 text-center b text-dark">
				<div class="col-12">
					<a href="#1" class="text-success">
						Lista de produtos
					</a>
					>>
					<a href="#2" class="text-success">
						<!-- Data -->
						Informações da compra
					</a>
					>>
					<a href="#3" class="text-dark">
						<!-- Check -->
						<b>Finalização</b>	
					</a>
				</div>
		</div>
		<!-- Container -->
		<div class="row justify-content-center">
			<div class="container-fluid bg-light p-1">
				<!-- Produtos -->
				<div class="row mb-1 bg-white border p-1 rounded">
					<!-- Title -->
					<div class="col-12  border bg-dark rounded p-1 text-center justify-content-center">
						<span class="h3 text-light">
							Produtos
						</span>
					</div>
					<div class="col-12 " id="list_of_products">
						<div class="col-12">
							<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
								<div class="col-2 border-right ">
									<span>
										7
									</span>
								</div>
								<div class="col-6">
									<span>
										<a href="#Produtobabal" class="text-dark">
											Produto tal de tal babal
										</a>
									</span>
								</div>
								<div class="col-4 border-left ">
									<span class="text-dark">
										R$ 120,00
									</span>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
								<div class="col-2 border-right ">
									<span>
										7
									</span>
								</div>
								<div class="col-6">
									<span>
										<a href="#Produtobabal" class="text-dark">
											Produto tal de tal babal
										</a>
									</span>
								</div>
								<div class="col-4 border-left ">
									<span class="text-dark">
										R$ 120,00
									</span>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
								<div class="col-2 border-right ">
									<span>
										7
									</span>
								</div>
								<div class="col-6">
									<span>
										<a href="#Produtobabal" class="text-dark">
											Produto tal de tal babal
										</a>
									</span>
								</div>
								<div class="col-4 border-left ">
									<span class="text-dark">
										R$ 120,00
									</span>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
								<div class="col-2 border-right ">
									<span>
										7
									</span>
								</div>
								<div class="col-6">
									<span>
										<a href="#Produtobabal" class="text-dark">
											Produto tal de tal babal
										</a>
									</span>
								</div>
								<div class="col-4 border-left ">
									<span class="text-dark">
										R$ 120,00
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-12 col-lg-8 bg-white border rounded p-1">
						<div class="col-12 mb-1 border bg-dark rounded p-1 text-center justify-content-center">
							<span class="h3 text-light">
								Endereço
							</span>
						</div>
						<div class="col-12  border bg-white rounded p-1 text-center justify-content-center">
							
						</div>
					</div>
					<div class="col-12 col-lg-4 bg-white border rounded p-1" >
						<div class="col-12 mb-1 border bg-dark rounded p-1 text-center justify-content-center">
							<span class="h3 text-light">
								Total
							</span>
						</div>
						<div class="col-12  border bg-white rounded p-1 text-center justify-content-center">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Calculo e Ações-->
		<div class="row justify-content-end">
			<!-- Ações -->
			<div class="col-12 col-lg-6 rounded justify-content-end text-center">
				<div class="btn-group mt-lg-5">
					<a class="btn btn-md btn-success" href="../../">
						Avançar >>>
					</a>
				</div>
			</div>
		</div>
	</section>
</body>
</html>