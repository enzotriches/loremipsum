<!DOCTYPE html>
<html>
<head>
	<!-- TITLE -->
	<title>Carrinho - Lorem Ipsum</title>
	<!-- CONFIGURATION  -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- CSS  -->
   	<link rel="stylesheet" href="../../../Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../../Assets/bin/css/cart.css"/>	
	<!-- JavaScript includes -->
	<script src="../../../Assets/bin/js/jquery-3.2.1.min.js"></script> 
	<script src="../../../Assets/bin/js/popper.js/dist/umd/popper.min.js"></script>
	<script src="../../../Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../../../Assets/bin/js/carrinho_dropdown.js"></script>
</head>

<body>

	<!-- Header -->
	<nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark  bg-dark" id="headerNav">
			<div class="container" id="ContainerHeader">
				<a class="navbar-brand a-logo" href="#">
					<img src="../../../Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
		   			<span class="h4">Lorem Ipsum</span>
		   		</a>
				
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
					<span class="navbar-toggler-icon"></span>
				</button>
			
				<div class="collapse navbar-collapse justify-content-md-center" id="navbarSite">
					
					<form action="#" class="form-inline ml-3 mr-0" id="FormHeader">
						<input type="text" class="form-control mr-2" id="buscar-header" type="search" placeholder="Buscar..." />
					</form>

					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							
							<a href="#" class="nav-link dropdown-toggle " data-toggle="dropdown" id="navDrop">
								Olá, random!
							</a>

							<div class="dropdown-menu">
								
								<a href="#Perfil" class="dropdown-item">Perfil</a>
								<a href="./Carrinho" class="dropdown-item">Carrinho <span class="badge badge-danger">3</span></a>
								<a href="#Notificações" class="dropdown-item">Notificações <span class="badge badge-secondary">1</span></a>
								<a href="#Sair" class="dropdown-item">Sair</a>

							</div>

						
						</li>
					</ul>

				</div>
			</div>
	</nav>	

	<!-- Container gatinho e sensual -->
	<section class="container border shadow p-3 box-cart mt-3 justify-content-center">
		<!-- Titulo ["Etapa"] -->
		<div class="row">
			<div class="col-12 text-center">
				<span class="h2 text-dark-50 b ">
					Entrega
				</span>
			</div>
		</div>
		<!-- Etapas ["Carrinho"] -->
		<div class="row pb-1 pt-2 text-center b text-dark">
				<div class="col-12">
					<a href="#1" class="text-success">
						<!--  -->
						Lista de produtos
					</a>
					>>
					<a href="#2" class="text-dark">
						<!-- Data -->
						<b>Informações de compra</b>	
					</a>
					>>
					<a href="#3" class="text-secondary">
						<!-- Check -->
						Finalização
					</a>
				
				</div>
		</div>
		<!-- Carrinho -->
		<div class="row justify-content-center">
			<!-- Escolha o endereço -->
			<div class="col-12 bg-light border rounded pt-2 pb-3 pl-lg-1 pr-lg-1">
				<div class="row justify-content-center text-center">

					<span class="h3 text-dark">Endereço de entrega:</span>
				</div>
				<div class="row justify-content-start text-start">
					
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Deep Web, Acre</b>
								<p>
									88960-000, R.Jucelino Rodolfo Santos - num (1543)
								</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Sombrio,SC</b>
								<p>
									88960-000, R.Jucelino Rodolfo Santos - num (1543)
								</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Porto Alegre,RS</b>
								<p>
									88960-000, R.Jucelino Rodolfo Santos - num (1543)
								</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Balneário Gaivota,SC</b>
								<p>
									88960-000, R.Jucelino Rodolfo Santos - num (1543)
								</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 border bg-white rounded ">
						<div class="col-12 p-4">
							<span class="h4">+</span>
							<a href="#" class="text-black" data-toggle="modal" data-target="#AddEndereco">
							 Adicionar endereço
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Escolha um método de entrega -->
			<div class="col-12 bg-light border rounded pt-2 pb-3 pl-lg-1 pr-lg-1">
				<div class="row justify-content-center text-center">

					<span class="h3 text-dark">Método de entrega:</span>
				</div>
				<div class="row justify-content-start text-start">
					
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Correio</b>
								<p>
									Sua entrega chegará entre:
								</p>
								<p>
									<span class="text-dark">
										13/03/19 - 20/03/19
									</span>
								</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Sedex</b>
								<p>
									Sua entrega chegará entre:
								</p>
								<p>
									<span class="text-dark">
										10/03/19 - 15/03/19
									</span>
								</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Guia - Lorem Ipsum</b>
								<p>
									Sua entrega chegará entre:
								</p>
								<p>
									<span class="text-dark">
										15/03/19 - 23/03/19
									</span>
								</p>
							</div>
						</div>
					</div>


				</div>
			</div>
			<!-- Escolha um método de pagamento -->
			<div class="col-12 bg-light border rounded pt-2 pb-3 pl-lg-1 pr-lg-1">
				<div class="row justify-content-center text-center">

					<span class="h3 text-dark">Método de pagamento:</span>
				</div>
				<div class="row justify-content-start text-start">
					
					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Correio</b>
								<p>
									Sua entrega chegará entre:
								</p>
								<p>
									<span class="text-dark">
										13/03/19 - 20/03/19
									</span>
								</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-4 col-md-6 p-lg-1 bg-white border bg-light rounded ">
						<div class="row">
							<div class="col-12 justify-content-start text-start">
								<input type="radio" class="form-control">
							</div>
							<div class="col-12">
								<b>Sedex</b>
								<p>
									Sua entrega chegará entre:
								</p>
								<p>
									<span class="text-dark">
										10/03/19 - 15/03/19
									</span>
								</p>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
		<!-- Calculo e Ações-->
		<div class="row justify-content-end">
			<!-- Ações -->
			<div class="col-12 col-lg-6 rounded justify-content-end text-center">
				<div class="btn-group mt-lg-5">
					<a class="btn btn-sm btn-secondary" href="../">
						<<< Voltar
					</a>
					<a class="btn btn-md btn-success" href="../Check/">
						Avançar >>>
					</a>
				</div>
			</div>
		</div>
	</section>

	<!-- MODALS -->
	<div class="modal fade" id="AddEndereco" tabindex="-1" role="dialog" aria-labelledby="AddEndereco" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content bg-dark text-light">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Cadastre um novo endereço!</h5>
	        
	        <button type="button text-light" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true" class="text-light ">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body bg-light" >

	     	 		<form action="#" method="POST" class="form-signin row justify-content-center m-1 bg-light rounded text-dark"  enctype="multipart/form-data">
	     	 			<div class="col-12">
	     	 				<!--  -->
	     	 				<div class="row border p-1 m-1">
	     	 					<div class="col-6 p-1">
		     	 					<label for="nome">Nome *:</label>
		     	 					<input type="text" class="form-control" name="nome" placeholder="Nome" />
				     	 		</div>
				     	 		<div class="col-6 p-1">
				     	 			<label for="referencia">Referência *:</label>
				     	 			<input type="text" class="form-control" name="referencia" placeholder="Referência" />
				     	 		</div>
			     	 		</div>
			     	 		<!--  -->
			     	 		<div class="row border p-1 m-1">
	     	 					<div class="col-6 p-1">
		     	 					<label for="valor">Valor *:</label>
		     	 					<input type="text" class="form-control" name="valor" placeholder="Valor" />
				     	 		</div>
				     	 		<div class="col-6 p-1">
				     	 			<label for="estoque">Peças em estoque *:</label>
				     	 			<input type="number" class="form-control" name="estoque" placeholder="Em estoque" />
				     	 		</div>
			     	 		</div>
			     	 		
	        			<input type="submit" class="btn btn-success" value="Cadastrar" name="goAdminProd">
	     	 		</form>


	     	 	
	      </div>
	      <div class="modal-footer bg-secondary rounded">
	      	<p class="text-light b">
	      		Lorem Ipsum Dolor sit Amet 
	      	</p>
	      </div>
	    </div>
	  </div>
	</div>

</body>
</html>