<!DOCTYPE html>
<html>
<head>
	<!-- TITLE -->
	<title>Carrinho - Lorem Ipsum</title>
	<!-- CONFIGURATION  -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- CSS  -->
   	<link rel="stylesheet" href="../../Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../Assets/bin/css/cart.css"/>	
	<!-- JavaScript includes -->
	<script src="../../Assets/bin/js/jquery-3.2.1.min.js"></script> 
	<script src="../../Assets/bin/js/popper.js/dist/umd/popper.min.js"></script>
	<script src="../../Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../../Assets/bin/js/carrinho_dropdown.js"></script>
</head>

<body>
	<!-- Header -->
	<nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark  bg-dark" id="headerNav">
			<div class="container" id="ContainerHeader">
				<a class="navbar-brand a-logo" href="#">
					<img src="../../Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
		   			<span class="h4">Lorem Ipsum</span>
		   		</a>
				
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
					<span class="navbar-toggler-icon"></span>
				</button>
			
				<div class="collapse navbar-collapse justify-content-md-center" id="navbarSite">
					
					<form action="#" class="form-inline ml-3 mr-0" id="FormHeader">
						<input type="text" class="form-control mr-2" id="buscar-header" type="search" placeholder="Buscar..." />
					</form>

					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							
							<a href="#" class="nav-link dropdown-toggle " data-toggle="dropdown" id="navDrop">
								Olá, random!
							</a>

							<div class="dropdown-menu">
								
								<a href="#Perfil" class="dropdown-item">Perfil</a>
								<a href="./Carrinho" class="dropdown-item">Carrinho <span class="badge badge-danger">3</span></a>
								<a href="#Notificações" class="dropdown-item">Notificações <span class="badge badge-secondary">1</span></a>
								<a href="#Sair" class="dropdown-item">Sair</a>

							</div>

						
						</li>
					</ul>

				</div>
			</div>
	</nav>	
	<!-- Container gatinho e sensual -->
	<section class="container border shadow p-3 box-cart mt-3 justify-content-center">
		<!-- Titulo ["Etapa"] -->
		<div class="row">
			<div class="col-12 text-center">
				<span class="h2 text-dark-50 b ">
					Carrinho
				</span>
			</div>
		</div>
		<!-- Etapas ["Carrinho"] -->
		<div class="row pb-1 pt-2 text-center b text-dark">
				<div class="col-12">
					<a href="#1" class="text-dark">
						<!-- View -->
						<b>Lista de produtos</b>
					</a>
					>>
					<a href="#2" class="text-secondary"> 
						<!-- Data -->
						Informações de compra
					</a>
					>>
					<a href="#3" class="text-secondary">
						<!-- Check -->
						Finalização	
					</a>
				</div>
		</div>
		<!-- Carrinho -->
		<div class="row justify-content-center">
			<!-- Produtos -->
			<div class="col-12 p-lg-1 border-top bg-light border-bottom rounded">
				<!-- | Qnt | Produto | Preço | - | -->
				<div class="row bg-dark text-light text-center border rounded pt-2 mt-2 pb-2 h5">
					<div class="col-2 border-right ">Qnt</div>
					<div class="col-4">Produto</div>
					<div class="col-4 border-left ">Preço</div>
					<div class="col-2 border-left ">...</div>
				</div>
				<!--     Lista de produtos -->
				<div class="row">
					<div class="col-12">
						<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
							<div class="col-2 border-right ">
								<span>
									7
								</span>
							</div>
							<div class="col-4">
								<span>
									<a href="#Produtobabal" class="text-dark">
										Produto tal de tal babal
									</a>
								</span>
							</div>
							<div class="col-4 border-left ">
								<span class="text-dark">
									R$ 120,00
								</span>
							</div>
							<div class="col-2 border-left justify-content-center text-center ">
								<!-- icones ['config', 'drop'] -->
								
								<a href="" class="p-1">
									<?php require_once('Assets/bin/images/icons/svg/settings_50 (1).svg'); ?>
								</a>
								<a href="" class="p-1">
									<?php require_once('Assets/bin/images/icons/svg/trash_50 (2).svg'); ?>
								</a>
								
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
							<div class="col-2 border-right ">
								<span>
									7
								</span>
							</div>
							<div class="col-4">
								<span>
									<a href="#Produtobabal" class="text-dark">
										Roupa bem bonita
									</a>
								</span>
							</div>
							<div class="col-4 border-left ">
								<span class="text-dark">
									R$ 92,00
								</span>
							</div>
							<div class="col-2 border-left justify-content-center text-center ">
								<!-- icones ['config', 'drop'] -->
								
								<a href="" class="p-1">
									<?php require_once('Assets/bin/images/icons/svg/settings_50 (1).svg'); ?>
								</a>
								<a href="" class="p-1">
									<?php require_once('Assets/bin/images/icons/svg/trash_50 (2).svg'); ?>
								</a>
								
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row bg-white text-center border rounded pt-2 pb-2 h5 text-dark">
							<div class="col-2 border-right ">
								<span>
									20
								</span>
							</div>
							<div class="col-4">
								<span>
									<a href="#Produtobabal" class="text-dark">
										Camiseta toda toda
									</a>
								</span>
							</div>
							<div class="col-4 border-left ">
								<span class="text-dark">
									R$ 103,99
								</span>
							</div>
							<div class="col-2 border-left justify-content-center text-center ">
								<!-- icones ['config', 'drop'] -->
								
								<a href="" class="p-1">
									<?php require_once('Assets/bin/images/icons/svg/settings_50 (1).svg'); ?>
								</a>
								<a href="" class="p-1">
									<?php require_once('Assets/bin/images/icons/svg/trash_50 (2).svg'); ?>
								</a>
								
							</div>
						</div>
					</div>
				</div>
					
			</div>
		</div>
		<!-- Calculo e Ações-->
		<div class="row justify-content-center">
			<!-- Calculo-->
			<div class="col-12 col-lg-6 border-right rounded justify-content-start text-start p-1">
				<!-- Bloco  -->
				<div class="container bg-light border">
					<!-- Aviso -->
					<div class="row justify-content-center">
						<small class="small text-center">
							<code class="text-secondary">
								Os valores pode mudar devido ao frete e o meio de pagamento.
							</code>
						</small>
					</div>
					<!-- Conta -->
					<div class="row">
						<div class="col-12">
							<div class="row border-bottom">
								<div class="col-6 text-center">
									<span class="h6 text-dark">
										Inicial: 
									</span>
								</div>
								<div class="col-6 text-center">
									<span class="h6 text-secondary">
										R$ 2530,00
									</span>
								</div>
							</div>
							<div class="row border-bottom">
								<div class="col-6 text-center">
									<span class="h6 text-dark">
										Desconto: 
									</span>
								</div>
								<div class="col-6 text-center">
									<span class="h6 text-secondary">
										- R$ 10,00
									</span>
								</div>
							</div>
							<div class="row  border-top">
								<div class="col-6 text-center">
									<span class="h4 text-dark">
										Total: 
									</span>
								</div>
								<div class="col-6 text-center">
									<span class="h4 text-dark">
										 R$ 2520,00
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Ações -->
			<div class="col-12 col-lg-6 border-left rounded justify-content-center text-center">
				<div class="btn-group mt-lg-5">
					<a class="btn btn-sm btn-secondary " href="../">
						<<< Voltar
					</a>
					<a class="btn btn-md btn-success" href="./Data/">
						Avançar >>>
					</a>
				</div>
			</div>
		</div>
	</section>

</body>
</html>