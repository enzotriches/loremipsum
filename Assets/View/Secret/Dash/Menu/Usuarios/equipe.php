<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Secret</title>
	
	<meta name="description" content="">
	<meta name="author" content="Enzo Trichês">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="../Assets/bin/css/secret.css"> 

	<link rel="stylesheet" type="text/css" href="../Assets/bin/css/pace.css"> 
    <link rel="stylesheet" href="../Assets/bin/js/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

  	<script src="../Assets/bin/js/jquery-3.2.1.min.js"></script>
	<script>
		$(document).ready(function() {
			var state = 1;
			$('#btn_menu').click(function() {
				if (state === 1) {
					$('#sidebar').css({
					    display: 'none'
					});
					$('#content').removeClass('col-lg-10');
					$('#content').addClass('col-lg-12');
					state--;
				}else if(state === 0){

					$('#content').removeClass('col-lg-12');
					$('#content').addClass('col-lg-10');

					$('#sidebar').css({
					    display: 'block'
					});
					state++;
				}else{
					window.href('#');
				}
			});
		});
	</script>	
</head>
<body>
<!-- Nav -->
	<nav class="navbar navbar-fixed-top bg-black">
	 	<div class="container " id="ContainerHeader">

			<a class="navbar-brand a-logo justify-content-start text-center" href="#">
				<img src="../Assets/bin/images/icons/bootstrap.png" width="35" height="35" class="d-inline-block align-top " alt="">
	   			<span class="h3 text-light b">Lorem Ipsum</span>
	   		</a>
	   		<a role="button" class="btn btn-black border-light text-light justify-content-center" data-toggle="collapse" data-target="#sidebar" aria-expanded="true" id="btn_menu">
              <i class="fas fa-bars text-light"></i> 
            </a>
		</div>
	</nav>

<!-- Wrapper (Menu, Main)   -->
	<div class="container-fluid">
	    <div class="row d-flex d-md-block flex-nowrap wrapper ">
	    	<!-- SIDEBAR -->
	        	        <div class="col-md-2 float-left col-2 pl-0 pr-0 collapse width show" id="sidebar">
	            <div class="list-group border-0 card text-center text-md-left"> 				 

	            <!-- Menu -->
	                <a href="" class="list-group-item d-inline-block collapsed">
	                	<i class="fa fa-tachometer-alt text-white"></i> 
	                	<span class="d-none d-md-inline">Menu</span>

	                </a>
				<!-- Produtos -->
	                <a href="#produtos" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fas fa-box text-warning"></i> 
	                	<span class="d-none d-md-inline">Produtos</span>
	                	<span class="badge badge-warning">2</span> 
	                </a>
	                	<!-- Data -->
		                <div class="collapse" id="produtos" data-parent="#sidebar">
		                    <a href="Colecoes" class="list-group-item">Coleções</a>
		                    <a href="Avisos" class="list-group-item">Avisos</a>
		                </div>
		        <!-- Pedidos -->
	                <a href="#pedidos" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fa fa-shopping-cart text-success"></i> 
	                	<span class="d-none d-md-inline">Pedidos</span>
	                	<span class="badge badge-success">14</span> 
	                </a>
						<!-- Menu -->
		                <div class="collapse" id="pedidos" data-parent="#sidebar">
		                	<!-- Compras -->
		                    <a href="#pedidos_compras" class="list-group-item" data-toggle="collapse" aria-expanded="false">Compras</a>
		                    <div class="collapse" id="pedidos_compras">
		                        <a href="Compras-Ativas" class="list-group-item" data-parent="#menu3sub2">Ativas</a>
		                        <a href="Compras-Concluidas" class="list-group-item" data-parent="#menu3sub2">Concluidas</a>
		                        <a href="Compras-Canceladas" class="list-group-item" data-parent="#menu3sub2">Canceladas</a>
		                    </div>
		                    <!-- Atendimento -->
		                    <a href="Atendimento" class="list-group-item" data-parent="#menu3">Atendimento</a>
		                </div>
		        <!-- Usuários -->
		            <a href="#usuarios" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fa fa-user text-primary"></i> 
	                	<span class="d-none d-md-inline">Usuários</span>
	                	<span class="badge badge-primary">6</span> 
	                </a>
		                <div class="collapse" id="usuarios" data-parent="#sidebar">
		                    <a href="Clientes" class="list-group-item" data-parent="#menu3">
		                    	Clientes
		                    </a>
		                    <a href="Equipe" class="list-group-item" data-parent="#menu3">
		                    	Equipe
		                    </a>
		                    <a href="You" class="list-group-item" data-parent="#menu3">
		                    	Você
		                    </a>
		                </div>
		        <!-- Marketing -->
		            <a href="#marketing" class="list-group-item d-inline-block collapsed" data-toggle="collapse" aria-expanded="false">
	                	<i class="fa fa-eye text-purple"></i> 
	                	<span class="d-none d-md-inline">Marketing</span> 
	                </a>
		                <div class="collapse" id="marketing" data-parent="#sidebar">
		                    <a href="Geral" class="list-group-item" data-parent="#marketing">Informações gerais</a>
		                    <a href="Cupoms" class="list-group-item" data-parent="#marketing">Cupoms</a>
		                    <a href="Email" class="list-group-item" data-parent="#marketing">Email</a>
		                    <a href="Social" class="list-group-item" data-parent="#marketing">Social</a>
		                </div>
	            <!-- Sair -->
	                <a href="#" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
	                	<i class="fa fa-door-open text-danger"></i> 
	                	<span class="d-none d-md-inline">Sair</span>
	                </a>
	            </div>
	        </div>
	        <!-- CONTEUDO -->
	        <main class="col-12 col-lg-10 float-left col-sm-11 px-5 pl-md-2 pt-2 main bg-light" id="content">
	        	<!-- Onde estou -->
	        	<div class="row">
	        		<!-- Titulo ['Option'] -->
	        		<div class="col-12">
	        			
			            <!-- Titulo ['Option'] -->
			            <div class="page-header">
			            	<i class="fa fa-user fa-2x text-dark">
			               	
			               </i>
				            <span class="h3">
				               
				                	Equipe
				            </span>
			            </div>
	        		</div>
	        		<!-- Olá e texto -->
	        		<div class="col-12">
			            
			            <p class="lead "> Alguma frase que faz sentido pra você.</p>	        			        		
	        		</div>
	        	</div>
	        		<hr>
	        	<!-- Boxes infos -->
	            <div class="row justify-content-center pt-1 pb-1 bg-light ">

	                <div class="col-5 col-lg-3 ml-1 p-1 bg-warning  rounded">
	                   		<p>
	                   			<span class="h5 border-bottom border-dark">
	                			<i class="fas fa-box text-dark"></i> 
	                   			Produtos
	                   			</span>
	                   		</p>
	                   		<p>
	                   			<span class="h6 text-dark">
	                   			Cadastrados:
	                   			</span>
	                   			<span class="h6 text-light">
	                   				542
	                   			</span>
	                   		</p>
	                   		<p>
	                   			<span class="h6 text-dark">
	                   			S/ Estoque:
	                   			</span>
	                   			<span class="h6 text-light">
	                   				3
	                   			</span>
	                   		</p>
	                </div>
	                <div class="col-5 col-lg-3 ml-1 p-1 bg-success  rounded">
	                  		<p>
	                   			<span class="h5 border-bottom border-dark">
	                			<i class="fas fa-shopping-cart text-dark"></i> 
	                   			Pedidos:
	                   			</span>
	                   		</p>
	                   		<p>
	                   			<span class="h6 text-dark">
	                   			Cadastrados:
	                   			</span>
	                   			<span class="h6 text-light">
	                   				542
	                   			</span>
	                   		</p>
	                   		<p>
	                   			<span class="h6 text-dark">
	                   			S/ Estoque:
	                   			</span>
	                   			<span class="h6 text-light">
	                   				3
	                   			</span>
	                   		</p>	
	                </div>
	                <div class="col-5 col-lg-3 ml-1 p-1 bg-primary  rounded">
	                   		<p>
	                   			<span class="h5 border-bottom border-dark">
	                			<i class="fas fa-user text-dark"></i> 
	                   			Usuários:
	                   			</span>
	                   		</p>
	                   		<p>
	                   			<span class="h6 text-dark">
	                   			Cadastrados:
	                   			</span>
	                   			<span class="h6 text-light">
	                   				542
	                   			</span>
	                   		</p>
	                   		<p>
	                   			<span class="h6 text-dark">
	                   			S/ Estoque:
	                   			</span>
	                   			<span class="h6 text-light">
	                   				3
	                   			</span>
	                   		</p>
	                </div>       
	            </div>
	            
	            <hr>
	        </main>
	    </div>    
	</div>

	<!--  -->
	<script src="../Assets/bin/js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../Assets/bin/js/pace.min.js"></script>
</body>
</html>