<?php 

  require_once('./Controllers/Route.php');

  //pega o valor da url que o servidor ta recebendo
  $uri = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

 //divide entre pastas
  $data = explode('/', $uri);
  
  array_push($data, '/');

 //    echo "<pre>";      	
	//   var_dump($data);  
	// echo "</pre>";     

  if (count($data) > 0) {
  	switch ($data[1]) {
		// INDEX
		case 'Index':
			Route::index();
			break;
		// SIGN IN
		case 'Entrada':
			switch ($data[2]) {
				case 'login':
					Route::login();
					break;
				case 'cadatro':
					Route::cadastro();
					break;				
				default:
					Route::login();
					break;
			}
			break;
		// SHOP
		case 'Shop':
			switch ($data[2]) {
				case 'Galeria':
					Route::shop();
					break;
				case 'Carrinho':
					switch ($data[3]) {
						case 'Data':
							Route::cart_entrega();
							break;
						case 'Check':
							Route::cart_entrega_pagamento();
							break;
						default:
							Route::cart();
							break;
					}
					break;				
				default:
					Route::shop();
					break;
			}
			break;
		// SECRET
		case 'Secret':
			switch ($data[2]) {
				// Produtos
				case 'Colecoes':
					switch ($data[3]) {
						case 'Produtos':
							Route::s_m_p_col_pro();
							break;
						
						default:
							Route::s_m_p_col();
							break;
					}
					break;
				case 'Avisos':
					Route::s_m_p_avisos();
					break;
				// Pedidos
				case 'Compras-Ativas':
					Route::s_m_c_cativas();
					break;
				case 'Compras-Concluidas':
					Route::s_m_c_cconcluida();
					break;
				case 'Compras-Canceladas':
					Route::s_m_c_ccancelada();
					break;
				case 'Atendimento':
					Route::s_m_c_atendimento();
					break;
				// Usuários
				case 'Clientes':
					Route::s_m_u_cliente();
					break;
				case 'Equipe':
					Route::s_m_u_equipe();
					break;
				case 'You':
					Route::s_m_u_you();
					break;
				// Marketing
				case 'Geral':
					Route::s_m_m_geral();
					break;
				case 'Cupoms':
					Route::s_m_m_cupons();
					break;
				case 'Email':
					Route::s_m_m_email();
					break;
				case 'Social':
					Route::s_m_m_social();
					break;

				default:
					Route::secret();
					break;
			}
			break;

		default:
			Route::index();
		break;
	}
  
  }else{
    Route::index();
  }

//REQUIRE SLIM
//  require_once('vendor/autoload.php');
// //START
//   $app = new \Slim\Slim(array(
// 	'mode' => 'development',
// 	'debug' => true,
// 	'templates.path' => './Assets/View',
// 	'cookies.encrypt' => true,
//     'cookies.lifetime' => '30 minutes',
//     'cookies.secure' => true,
//     'cookies.httponly' => true
//  ));
//  $pastas = array(
//  	'index'=>array('location' => './Assets/View/'),
//  );


//   $app->get('/',function() use ($app){
//   	$app->render('index.php');
//   });
//   $app->get('/Entrada',function() use ($app){
//   	$app->render('Entrada/index.php');
//   });
//   $app->get('/Secret',function() use ($app){
//   	$app->render('login.php');
//   });
//   $app->get('/Secret/:menu/:item', function($menu, $item) use ($app){echo "Hi, ".$menu." and ".$item;});




//   $app->get('/hello/:name',function ($name){
//   	echo "Hello,".$name;
//   });

//   $app->run();

?>