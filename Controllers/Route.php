<?php 

class Route{

	protected function __construct(){

	}

    // START
	public static function index(){
		require_once('./Assets/View/index.php');
	}
	public static function login(){
		require_once('./Assets/View/Entrada/index.php');
	}
	public static function cadastro(){
		require_once('./Assets/View/Entrada/cadastro.php');
	}

	// SHOP
 	public static function shop(){
 		require_once('./Assets/View/Shop/index.php');
 	}

 	public static function cart(){
 		require_once('./Assets/View/Shop/Carrinho/index.php');
 	}

 	public static function cart_entrega(){
 		require_once('./Assets/View/Shop/Carrinho/entrega.php');
 	}

 	public static function cart_entrega_pagamento(){
 		require_once('./Assets/View/Shop/Carrinho/pagamento.php');
 	}

 	// Secret
 	
	public static function secret(){
		require_once('./Assets/View/Secret/Dash/index.php');
	}
		// Produtos 
	 	public static function s_m_p_avisos(){
 			require_once('./Assets/View/Secret/Dash/Menu/Produtos/aviso.php');
 		}
	 	public static function s_m_p_col(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Produtos/col.php');
	 	}

	 	public static function s_m_p_col_pro(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Produtos/produtos/teste.php');
	 	}

	 	// Pedidos c
	 	public static function s_m_c_cativas(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Pedidos/c_ativa.php');
	 	}
	 	public static function s_m_c_cconcluida(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Pedidos/c_concluida.php');
	 	}
	 	public static function s_m_c_ccancelada(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Pedidos/c_cancelada.php');
	 	}
	 	public static function s_m_c_atendimento(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Pedidos/atendimento.php');
	 	}

	 	// Usuários
	 	public static function s_m_u_cliente(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Usuarios/clientes.php');
	 	}
	 	public static function s_m_u_equipe(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Usuarios/equipe.php');
	 	}
	 	public static function s_m_u_you(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Usuarios/you.php');
	 	}

	 	// Marketing
 		public static function s_m_m_geral(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Marketing/geral.php');
	 	}
	 	public static function s_m_m_cupons(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Marketing/cupons.php');
	 	}

	 	public static function s_m_m_email(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Marketing/email.php');
	 	}
	 	public static function s_m_m_social(){
	 		require_once('./Assets/View/Secret/Dash/Menu/Marketing/social.php');
	 	}


}

?>