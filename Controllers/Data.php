<?php 
class Data extends PDO {

	private $conn;
	private $config = array(

		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
	    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
	    PDO::ATTR_CASE => PDO::CASE_LOWER,
	    PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING

	);

	public function __construct(){
		try {
			$this->conn = new PDO('mysql:host=localhost;dbname=ecommerce', 'root', '', $this->config);

		} catch (PDOException $e) {
			$this->conn = 
			    exit('
					<center>
						<h1>Falha ao conectar no banco de dados! Volte mais tarde!</h1>
						<h3>'.date('d/m/Y h:i:s').'</h3>
					</center>
				');
			//send email

			
		}
		return $this->conn;
		
	}
	private function setParams($statment, $parameters = array()){
		foreach ($parameters as $key => $value) {
			$this->setParam($statment, $key, $value);
		}
	}
	private function setParam($statment, $key, $value){
		$statment->bindParam($key, strip_tags($value));
	}
 
	public function procede($rawQuery, $params = array(), $callback=false){
		$stmt = $this->conn->prepare($rawQuery);

		$this->setParams($stmt, $params);

		if ($stmt->execute()) {
			return $stmt;
		}else{
			return false;
		}
		
	}

	


}

?>